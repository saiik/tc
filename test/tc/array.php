<?php

function pretty($m) {
	echo "<pre>";
	print_r($m);
	echo "</pre>";
}

class arrays {

	var $_depth;

	public function depth(array $array, $first = true, $oldKey = "", $level = 0, $firstLevel = null) {
		if(empty($array)) return false;
		
		if($first === true) $this->_depth = array();

		$depthLevel = ($level==0?0:$level);
		foreach($array as $key => $value) {
			if(is_array($value)) {
				$depthLevel++;
				if(empty($oldKey)) $oldKey = "";
				$this->depth($value, false, $key, $depthLevel, $oldKey);
			} else {
				if($firstLevel != "") {
					$dotKey = $firstLevel.".";
				} else $dotKey = "";

				$dotKey .= ($oldKey!=""?$oldKey.".".$key:$key);
				
				$this->_depth[$dotKey] = $value;
			}
		}

		return $this->_depth;
	}

}

$test = new arrays();