<?php
/**
 * TumbCodes System
 * @version 0.2.0 DEV <Build 020>
 * @author Tobias Fuchs
 * @copyright Tobias Fuchs
 * @website http://schockma.com
 * @status development
 **/

/**
 * start sessions
 *
 *
 **/
session_start();

/**
 * sets the default zime zone
 *
 * to get rid of the warning
 **/
date_default_timezone_set("Europe/Berlin");

/**
 * set default charset to utf8
 *
 * to use german symbols
 **/
header('Content-Type: text/html; charset=utf-8');

/***
 * define the base paths
 * important to load some functionns
 * without the autoloader
 *
 ***/
if(!defined('TC_BASE')) {
	define('TC_BASE', dirname(__FILE__).DIRECTORY_SEPARATOR);
	define('TC_CORE', TC_BASE.'core'.DIRECTORY_SEPARATOR);
	define('TC_FILES', TC_BASE.'files'.DIRECTORY_SEPARATOR);
	define('TC_DB', TC_BASE.'database'.DIRECTORY_SEPARATOR);
	define('TC_EXCEPTION', TC_BASE.'exception'.DIRECTORY_SEPARATOR);
	define('TC_SMARTY', TC_BASE.'smarty'.DIRECTORY_SEPARATOR);
}

/***
 * checks if the current version is at least php 5.3.0
 *
 * tumbcodes needs at least php 5.3.0
 ***/
if(version_compare(PHP_VERSION, '5.3.0', '<')) {
	die('<strong>You need at least PHP 5.3.0</strong>');
}

/**
 * autoloader
 * loads automaticly classes
 * pretty simple
 * all classes are pre defined currently
 * probably switch to a better autoloader soon
 **/
require_once(TC_BASE."library/autoload.php");

/***
 * autoloader for third party code
 *
 * use ./composer install or ./composer update to install 
 **/
require_once(TC_BASE."vendor/autoload.php");

/**
 * @function pretty
 * @param variable
 *
 * prints out a pretty version of var_dump
 **/
if(!function_exists('pretty')) {
	function pretty($i) {
		echo "<pre>";
		var_dump($i);
		echo "</pre>";
		die;
	}
}

/***
 * init the main system
 * load instances
 * set the environment
 * set instances
 **/
TumbCodes::init(["env" => "develop", "cache" => false, "update" => false]);
