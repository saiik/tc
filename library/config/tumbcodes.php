<?php

return [
	'database' => [
		'driver' => 'MySQL',
		'host' => 'localhost',
		'user' => 'root',
		'pass' => 'vivi',
		'base' => 'fof',
	],
	'template' => 'default',
	'cache' => [
		'template' => 'cache/tmp',
		'database' => 'cache/database'
	]
];
