<?php

class String {

	/**
	 * @private int
	 *
	 * all numbers from 0 to 10
	 **/
	private static $int = array(
			1 => '1',
			2 => '2',
			3 => '3',
			4 => '4',
			5 => '5',
			6 => '6',
			7 => '7',
			8 => '8',
			9 => '9',
			0 => '0'	
		);

	/**
	 * @private letters
	 *
	 * all letters from a to z
	 **/
	private static $letters = array(
		'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i',
		'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't',
		'u', 'v', 'x', 'y', 'z', 'ö', 'ä', 'ü'
		);

	/**
	 * @public boolToString
	 * @param boolean bool
	 * 
	 * converts a boolean to string
	 *
	 * @return string
	 **/
	public static function boolToString($bool) {
		if(!isset($bool)) return false;

		if($bool === true) {
			return "true";
		}

		return "false";
	}

	/**
	 * @public stringToBool
	 * @param string string
	 *
	 * converts an string to an bool
	 * 
	 * @return boolean
	 **/
	public static function stringToBool($string="") {
		if(empty($string)) return false;

		if($string == "true" || $string == "1") {
			return true;
		}

		return false;
	}

	/**
	 * converts an integer to an string
	 *
	 * @param int int
	 * @return string
	 **/
	public static function castIntToString($int) {
		if(!isset($int)) return false;

		return (string)$int;
	}

	/**
	 * casts a string to an integer
	 *
	 * @param string string
	 * @return int
	 **/
	public static function castStringToInt($string) {
		if(!isset($string)) return false;

		$string = str_replace(self::$letters, "", $string);

		$string = (int)$string;

		if(is_int($string) && $string != 0) {
			return $string;
		} else {
			throw new Exception("String couldn't get converted to integer", "502");
		}
	}

	public static function camelCase($string = "") {
		if(empty($string)) return false;

		if(($str = str_replace("_", "", $string) != $string)) $input = Arrays::extractString("_", $string);
		elseif(($str = str_replace(" ", "", $string) != $string)) $input = Arrays::extractString(" ", $string);
		elseif(($str = str_replace(".", "", $string) != $string)) $input = Arrays::extractString(".", $string);
		else return $string;
		
		$output = "";
		$i = 0;
		foreach($input as $word) {
			if($i == 0) $output .= $word;
			else $output .= ucfirst($word);
			$i++;
		}

		return $output;
	}

	public static function escape($string="") {
		if(empty($string)) return false;

		$string = htmlspecialchars($string, ENT_QUOTES);

		$db = app("database");

		$string = $db->escape($string);

		return $string;
	}
}