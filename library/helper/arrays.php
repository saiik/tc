<?php

class Arrays {

	protected static $_depth = array();
	
	/**
	 * @public emptyArray
	 * @param array array
	 *
	 * deletes all empty elements in an array
	 *
	 * @return array
	 **/
	public static function emptyArray(array $array) {
		foreach($array as $key) {
			if(!empty($key)) $new[] = $key;
		}

		return $new;
	}

	/**
	 * @public changeArray
	 * @param array name
	 * @param array new
	 *
	 * gives an array new elements
	 *
	 * @return array
	 **/
	public static function changeArray(array $name, array $new) {
		$array = $name;
		if(!is_array($array)) return false;
	
		if(is_array($new)) {
			foreach($new as $key => $value) {
				if(array_key_exists($key, $array)) {
					$array[$key] = $value;
				} else {
					return false;
				}
			}
		} else {
			return false;
		}

		return $array;
	}

	/**
	 * @public getFirstKey
	 * @param array array
	 *
	 * returns the first key of an element
	 *
	 * @return string
	 **/
	public static function getFirstKey(array $array) {
		if(empty($array)) return false;
		reset($array);

		return key($array);
	}

	/** 
	 * push_array
	 * push elements into an array
	 *
	 * @param   array value
	 * @param   string $name
	 * @return  bool
	 **/
	public function pushArray($name=null, array $value) {

		$array = $name;
		if(!is_array($array)) return false;

		if(is_array($value)) {
			foreach($value as $key => $val) {
				if(array_key_exists($key, $array)) {
					return false;
				} else {
					$array[$key] = $val;
				}
			}
		} else {
			return false;
		}

		return $array;
	}

	/**
	 * explode an string to an array
	 * @param  string $i
	 * @param  string $string 
	 * @return array        
	 */
	public static function extractString($i, $string) {
		if(empty($i) || empty($string)) return false;

		if(strlen($string) > 1) {
			$tmp = explode($i, $string);

			if(count($tmp) > 1) {
				return $tmp;
			} else return $string;
		}

		return $string;
	}

	/**
	 * converts 3 dimensional array to string array
	 * example: [test => [test2 => [test3 => success]]] to test.test2.test3 = success
	 *
	 * @param array $array
	 * @param boolean $first
	 * @param string $oldKey
	 * @param integer $level
	 * @param strng firstLevel
	 * @return array
	 **/
	public static function depth(array $array, $first = true, $oldKey = "", $level = 0, $firstLevel = null) {
		if(empty($array)) return false;
		
		if($first === true) self::$_depth = array();

		$depthLevel = ($level==0?0:$level);
		foreach($array as $key => $value) {
			if(is_array($value)) {
				$depthLevel++;
				if(empty($oldKey)) $oldKey = "";
				self::depth($value, false, $key, $depthLevel, $oldKey);
			} else {
				if($firstLevel != "") {
					$dotKey = $firstLevel.".";
				} else $dotKey = "";

				$dotKey .= ($oldKey!=""?$oldKey.".".$key:$key);
				
				self::$_depth[$dotKey] = $value;
			}
		}

		return self::$_depth;
	}
}