<?php

class BBCode {

	protected static $_simple = [
		'~\[b\](.*?)\[/b\]~s',
		'~\[i\](.*?)\[/i\]~s',
		'~\[u\](.*?)\[/u\]~s',
		'~\[size=(.*?)\](.*?)\[/size\]~s',
		'~\[color=(.*?)\](.*?)\[/color\]~s' 
	];

	protected static $_simpleReplace = [
		'<b>$1</b>',
		'<i>$1</i>',
		'<span style="text-decoration:underline;">$1</span>',
		'<span style="font-size:$1px;">$2</span>',
		'<span style="color:$1;">$2</span>' 
	];

	public static function parse($text = null) {
		if(is_null($text)) return false;

		return preg_replace(static::$_simple, static::$_simpleReplace, $text);
	}

}