<?php

class Util {

	/**
	 * contains all letters and digits
	 *
	 * @param array
	 **/
	protected static $letters = array(
		'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
		'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
		'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
	);

	/**
	 * generates a complete dynmaic hash
	 * 
	 * @return string
	 */
	public static function generateUniqHash() {
		$pack1 = hash_hmac("sha1", md5(str_shuffle('ABcLxMA93JXnxM332x§9LX;"')), md5(str_shuffle('(3nx/3bx!xn5jxairsamd')));
		$pack2 = hash_hmac("sha1", md5(str_shuffle('3c23hx3tgsc21ayc463435c;"')), md5(str_shuffle('(131cs45hj656dfdfadsxy')));

		return hash_hmac("whirlpool", pack("h*", md5(hash_hmac("sha1", pack("h*", $pack1.$pack2), pack("h*", $pack2.$pack1)))), $pack2);
	}

	/**
	 * generates a uniq form a id
	 *
	 * @param  int 
	 * @return string
	 **/
	public static function generateUniqFromId($id="") {
		if(empty($id) && $id < 10000) return false;

		$url = "";

		while($id > 0) {
			$remain = $id%62;
			$url = $url . self::$letters[$remain];
			$id = floor($id/62);
		}

		return $url;
	}

	/**
	 * returns the base url of the site
	 *
	 * @return string
	 **/
	public static function getBaseUrl() {
		$srv = $_SERVER;

		$string = explode("index", $srv["PHP_SELF"]);

		return $srv["HTTP_HOST"].$string[0];
	}

	/**
	 * returns the redirect url for redirects (lol)
	 *
	 * @param string
	 * @return string
	 **/
	public static function getUrl($url = "") {
		if(empty($url)) return false;

		return "http://".self::getBaseUrl().$url;
	}

	/**
	 * checks if the referer is from the site or from external site
	 *
	 * @param booelan
	 * @return string | boolean
	 **/
	public static function referer($boolean = false) {
		if(isset($_SERVER["HTTP_REFERER"])) {
			$refer = $_SERVER["HTTP_REFERER"];

			if(strpos($refer, $_SERVER["HTTP_HOST"]) !== false) {
				return ($boolean?true:$refer);
			}
		} 

		return false;
	}

	public function generateBreadcrumb(array $breads) {
		$string = "";

		$count = (count($breads) - 1);
		$i = 0;
		foreach($breads as $bread) {
			if($i < $count) {
				$string = $string . $bread . " / ";
			} else {
				$string = $string . $bread;
			}

			$i++;
		}

		return $string;
	}
}