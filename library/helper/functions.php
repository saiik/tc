<?php

/**
 * shorter method to call an instance
 *
 * @param string $name
 * @return object
 **/
if(!function_exists('app')) {
	define('MASTER', 'master');

	function app($name = null) {
		if(empty($name)) return false;

		return TumbCodes::get($name);
	}
}

/**
 * returns the kernel instance
 *
 * @return object
 **/
if(!function_exists('kernel')) {

	function kernel() {
		return TumbCodes::kernel();
	}

}