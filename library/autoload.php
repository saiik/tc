<?php

function tcLoad($class="") {
	if(empty($class)) return false;

	$classmap = array(
			"TumbCodes" => dirname(__FILE__)."/core/tumbcodes.php",
			"Config" => dirname(__FILE__)."/core/config.php",
			"MySQL" => dirname(__FILE__)."/core/database/mysql.php",
			"SQLite" => dirname(__FILE__)."/core/database/sqlite.php",
			"Mongo" => dirname(__FILE__)."/core/database/mongo.php",
			"Smarty" => dirname(__FILE__)."/third/smarty/Smarty.class.php",
			"Files" => dirname(__FILE__)."/core/files.php",
			"Router" => dirname(__FILE__)."/core/router/router.php",
			"Modules" => dirname(__FILE__)."/core/modules.php",
			"Server" => dirname(__FILE__)."/core/server.php",
			"Cache" => dirname(__FILE__)."/core/cache.php",
			"Versions" => dirname(__FILE__)."/core/versions.php",
			"Arrays" => dirname(__FILE__)."/helper/arrays.php",
			"TC_Exception" => dirname(__FILE__)."/core/exception/exception.php",
			"Render" => dirname(__FILE__)."/core/render.php",
			"Driver" => dirname(__FILE__)."/core/database/driver.php",
			"Hooks" => dirname(__FILE__)."/core/hooks.php",
			"Util" => dirname(__FILE__)."/helper/util.php",
			"Debug" => dirname(__FILE__)."/core/debug.php",
			"BBCode" => dirname(__FILE__)."/helper/bbcode.php",
			"Admin" => dirname(__FILE__)."/model/admin.php",
			"Language" => dirname(__FILE__)."/core/language.php",
			"Package" => dirname(__FILE__)."/core/package.php",
			"Controller" => dirname(__FILE__)."/core/interface/controller.php",
			"Kernel" => dirname(__FILE__)."/core/kernel.php",
			"Controller" => dirname(__FILE__)."/core/controller.php",
			"Model" => dirname(__FILE__)."/core/model.php",
			"Container" => dirname(__FILE__)."/core/container.php",
		);

	if(isset($classmap[$class])) {
		if(file_exists($classmap[$class])) {
			require_once($classmap[$class]);
		}
	} else {
		if(file_exists(dirname(__FILE__)."/helper/".strtolower($class).".php")) {
			require_once(dirname(__FILE__)."/helper/".strtolower($class).".php");
		}
	}

	return false;
}

function tcLoadInterface($name = null) {
	$name = strtolower($name);
	$name = str_replace("Interface", "", $name);

	if(file_exists(dirname(__FILE__)."/core/interface/".$name.".php")) {
		require_once(dirname(__FILE__)."/core/interface/".$name.".php");
	}

	return false;
}

spl_autoload_register("tcLoadInterface");

spl_autoload_register("tcLoad");

function tcLoadControllers($classInput = null) {
	if(empty($classInput)) return false;

	$classTmp = str_replace("Controller", "", $classInput);

	if(file_exists("application/controllers/".$classTmp.".php")) {
		require_once("application/controllers/".$classTmp.".php");
	} else {
		if(file_exists("library/controller/".$classTmp.".php")) {
			require_once("library/controller/".$classTmp.".php");
		}
	}

	return false;
}

function tcLoadModels($classInput = null) {
	if(empty($classInput)) return false;

	$class = Arrays::extractString("_", $classInput);

	if(is_array($class)) {
		$classTmp = $class[0];
	} else $classTmp = $class;
	
	$class_ = strtolower($classTmp);
	if(file_exists("app/model/".$class_.".php")) {
		require_once("app/model/".$class_.".php");
	} else {
		if(file_exists("library/model/".$classTmp.".php")) {
			require_once("library/model/".$classTmp.".php");
		}
	}

	return false;
}

spl_autoload_register("tcLoadControllers");
spl_autoload_register("tcLoadModels");

function tcLoadMappers($mapper = null) {
	if(empty($mapper)) return false;

	$class = str_replace("Mapper", "", $mapper);

	if(file_exists("library/mapper/".$class.".php")) {
		require_once("library/mapper/".$class.".php");
	} else {
		return false;
	}
}

spl_autoload_register("tcLoadMappers");

function tcLoadObject($mapper = null) {
	if(empty($mapper)) return false;

	$class = str_replace("Object", "", $mapper);

	if(file_exists("library/object/".$class.".php")) {
		require_once("library/object/".$class.".php");
	} else {
		return false;
	}
}

spl_autoload_register("tcLoadObject");
