<?php

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Intervention\Image\ImageManager;

class Kernel implements KernelInterface {

	/**
	 * current env status
	 *
	 * @param string
	 **/
	private $_env;

	/**
	 * cache disabled or enabled?
	 *
	 * @param boolean
	 **/
	private $_cache;

	/**
	 * do automaticly udpates or not
	 *
	 * @param boolean
	 **/
	private $_update;

	/**
	 * holds container object
	 *
	 * @param object
	 **/
	private $_container;

	/**
	 * kernel informations
	 *
	 * @param array
	 **/
	private $kernel = [
		'version' => '001',
		'author' => 'Tobias Fuchs',
		'email' => 'saikon@hotmail.de'
	];

	/**
	 * debug or not
	 *
	 * @param boolean
	 **/
	protected $_debug;

	/**
	 * magic method
	 * loads container and set up base configuration
	 *
	 * @param array $env
	 **/
	public function __construct(array $env) {
		$this->_env = $env["env"];
		$this->_cache = $env["cache"];
		$this->_update = $env["update"];

		if($this->_env == "develop") {
			ini_set("display_errors", 1);
			error_reporting(E_ALL);
			$this->debug = true;
		} else {
			ini_set("display_errors", 1);
			error_reporting(0);
			$this->debug = false;
		}

		$this->_container = new Container();

		return $this;
	}

	/**
	 * starts the tumbcodes system
	 * registers all important instances
	 *
	 * @return void
	 ***/
	public function boot() {
		if(!$this->_container instanceof Container) {
			throw new Exception("Container class needs to be created", "502");
		}

		// configuration loader
		$this->make("config", function() {
			return new Config();
		});

		// simple file loader
		$this->make("file", function() {
			$file = new Files();

			$file->load("library/helper/functions"); // load app functions

			return $file;
		});

		// file system needs to be created before loading configurations
		$config = $this->singleton("config");
		$config->loadConfiguration("tumbcodes", "tc");

		// template engine
		$this->make("smarty", function() use($config) {
			$stack = $config->getConfiguration("tc");

			$smarty = new Smarty();

			$smarty->setTemplateDir(TC_BASE.'templates'.DIRECTORY_SEPARATOR.$stack["template"].DIRECTORY_SEPARATOR);
			$smarty->setCompileDir($stack["cache"]["template"]);
			$smarty->setCacheDir($stack["cache"]["template"]);

			return $smarty;
		});

		// data base engine
		$this->make("database", function() use($config) {
			$stack = $config->getConfiguration("tc");

			$driver = $stack["database"]["driver"];
			unset($stack["database"]["driver"]);

			$database = new $driver($stack["database"]);

			return $database;
		});

		// language system
		$this->make("language", function() {
			$language = new Language();

			$language->loadLanguage("user"); // load important language packs
			$language->loadLanguage("page");
			$language->loadLanguage("auth");

			return $language;
		});

		// image library
		$this->make("image", function() {
			return new ImageManager(["driver" => "imagick"]);
		});

		// event system
		$this->make("hooks", function() {
			$hook = new Hooks();

			$hook->registerHook("kernel.boot"); // register some hooks
			$hook->registerHook("kernel.shutdown");
			$hook->registerHook("init.end");
			$hook->registerHook("render.display");

			return $hook;
		});

		// rendering tumbcodes
		$this->make("render", function() {
			return new Render();
		});

		// route through tumbcodes :D
		$this->make("router", function() {
			$route = new Router();
			$route->get(["/", "HomeController::init"]);

			return $route;
		});

		// server manager
		$this->make("server", function() {
			return new Server();
		});

		$this->singleton("server")->setSession([
			'env' => [
				'name' => $this->_env,
				'cache' => String::boolToString($this->_cache), // casting bool to string
				'update' => String::boolToString($this->_update)
			],
		], true);

		// cache system
		$this->make("cache", function() use($config) {
			// we already loaded while creating the database
			$stack = $config->getConfiguration("tc");

			return new Cache($stack["cache"]);
		});

		$this->singleton("hooks")->fireEvent("kernel.boot");
	}

	/**
	 * shuts down the system
	 *
	 * @return void
	 **/
	public function shutdown() {
		$this->singleton("hooks")->fireEvent("kernel.shutdown");

		$this->_container->detach("", true);
	}

	/**
	 * returns a single instance
	 *
	 * @param string $name
	 * @return object
	 **/
	public function singleton($name = null) {
		return $this->_container->get($name);
	}

	/**
	 * register a new singletone
	 *
	 * @param string $name
	 * @param function $callback
	 * @return object
	 **/
	public function make($name = null, $callback = null) {
		if(is_callable($callback) AND !empty($name)) {
			return $this->_container->register($name, $callback);
		} else {
			throw new Exception("Please provide correct parameters", "502");
		}
	}

	/**
	 * magic method
	 *
	 * @return string
	 **/
	public function __toString() {
		return "Kernel version 0.0.1";
	}

	/**
	 * returns debug
	 *
	 * @return boolean
	 **/
	public function isDebug() {
		return $this->debug;
	}

}