<?php

/**
 * Interface for Kernel classes
 *
 * Use this for your own Kernel class to provide correct booting
 *
 * @param interface
 **/
interface KernelInterface {

	/**
	 * boots tumbcodes
	 * Creates instances and configures them
	 *
	 * important
	 *
	 * @param -
	 * @return void
	 **/ 
	public function boot();

	/**
	 * deletes all instances and resets server configurations
	 *
	 * used to shutdown tumbcodes system
	 *
	 * @param -
	 * @return void
	 **/
	public function shutdown();

	/**
	 * Returns an instance of an object 
	 *
	 * @param string $name (of object)
	 * @return object
	 **/
	public function singleton($name = null);

	/**
	 * Creates an instance of an object
	 *
	 * @param string $name (of object)
	 * @param callable $callback
	 * @return object
	 **/
	public function make($name = null, $callback = null);

}