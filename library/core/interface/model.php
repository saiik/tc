<?php

/**
 * Interface for model classes
 *
 * Use this for your own model class
 *
 * @param interface
 **/
interface ModelInterface {

	/**
	 * Used for configurate basic variables
	 *
	 * @param -
	 * @return void
	 **/
	public function configure();

}