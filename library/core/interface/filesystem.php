<?php

/**
 * Interface for filesystem classes
 *
 * Use this for your own Filesystem class to provide correct file loading
 *
 * @param interface
 **/
interface FileSystem {

	/**
	 * method to load files
	 *
	 * Loads files directly into a variable
	 *
	 * @param string $file (filename)
	 * @param string $type (file ending eq. php/js/tpl etc.)
	 * @return file
	 **/
	public function load($file = null, $type = "php");

	/**
	 * method to check if file exists
	 *
	 * Can check if file exists or if an dir exists
	 *
	 * @param string $file (filename)
	 * @param string $type (file ending eq. php/js/tpl etc.)
	 * @return boolean
	 **/
	public function check($file = null, $type = "php");

}