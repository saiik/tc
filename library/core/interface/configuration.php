<?php

/**
 * Interface for configuration classes
 *
 * Use this for your own configuration class to provide correct configuration loading
 *
 * @param interface
 **/
interface Configuration {

	/**
	 * method to load configurations files
	 * Automaticly loads configuration file and stores them in a stack
	 *
	 * @param string $file (filename)
	 * @return void
	 **/
	public function loadConfiguration($file = null);
	
	/**
	 * method to get configuration
	 * Returns a configuration by name from a stack
	 *
	 * @param string $name (configuration name in stack)
	 * @return array
	 **/
	public function getConfiguration($name = null);

}