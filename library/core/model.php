<?php

abstract class Model implements ModelInterface {

	/**
	 * holds the smarty object
	 *
	 * @param object
	 ***/
	protected $smarty;
	
	/**
	 * holds the database object
	 *
	 * @param object
	 **/
	protected $database;

	/**
	 * holds the language object
	 *
	 * @param object
	 **/
	protected $language;

	/**
	 * run configure function
	 *
	 * @return void
	 **/
	public function __construct() {
		$this->configure();
	}

	/**
	 * Configuration function.
	 * This function gives some basic variables you can use in your model.
	 * So you don't need to create this everytime
	 * 
	 * @return void
	 **/
	public function configure() {
		try {
			$this->smarty = app("smarty");
			$this->database = app("database");
			$this->language = app("language");
		} catch(Exception $e) {

		}
	}
}