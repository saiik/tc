<?php

class Container {

	/**
	 * stores all instances in an array
	 *
	 * @param array
	 **/
	private $_containers = [];

	/**
	 * stores last called instance
	 *
	 * @param string
	 **/
	private $_lastCall;

	/**
	 * displays container object
	 *
	 * @return void
	 **/
	public function debug() {
		if(kernel()->isDebug()) {
			pretty($this);
		}
	}

	/**
	 * registers a single instance
	 * 
	 * @param string $name
	 * @param function $callback
	 * @return boolean
	 **/
	public function register($name = null, $callback) {
		if(empty($name) || empty($callback)) return false;

		if(array_key_exists($name, $this->_containers)) {
			return $this;
		} else {
			try {
				$this->_containers[$name] = call_user_func($callback);

				return true;
			} catch(Exception $e) {
				pretty($e->getMessage());
			}
		}
	}

	/**
	 * returns a single instance of an object
	 *
	 * @param string $name
	 * @param boolean $create
	 * @return object
	 **/
	public function get($name, $create = false) {
		if(empty($name)) return false;

		if(array_key_exists($name, $this->_containers)) {
			return $this->_containers[$name];
		} else {
			if($create === true) {
				try {
					$name = ucfirst($name);

					if(class_exists($name)) {
						$tmpObject = new $name();
					}

					if(is_object($tmpObject) AND $tmpObject instanceof $name) {
						$this->_containers[$name] = $tmpObject;

						return $tmpObject;
					}  else {
						throw new TC_Exception("Object couldn't get created: \\\\".$name);
					}
				} catch(Exception $e) {
					$this->_lastCall = $name;

					return $this;
				} catch(TC_Exception $tc) {
					return;
				}
			} else {
				return false;
			}
		}
	}

	/**
	 * can call an model
	 *
	 * @return object
	 **/
	public function model() {
		if(empty($this->_lastCall)) return false;

		return $this->get($this->_lastCall."Model");
	}

	/**
	 * can call an controller
	 *
	 * @return object
	 **/
	public function controller() {
		if(empty($this->_lastCall)) return false;

		return $this->get($this->_lastCall."Controller");
	}

	/**
	 * removes all instances or a single instance
	 *
	 * @param string $name
	 * @param boolean $all
	 * @return void
	 **/
	public function detach($name = null, $all = false) {
		if(empty($name) && $name != "") return false;

		if(!$all && $name != "") {
			if(isset($this->_containers[$name])) {
				unset($this->_containers[$name]);
			}
		} else {
			$this->_containers = [];
		}

		return;
	}

}