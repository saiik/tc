<?php

class Config implements Configuration {

	private $_configStack = [];

	public function __construct() {

	}

	public function loadConfiguration($file = null, $out = null) {
		if(empty($file)) return false;

		if($tmpConfig = TumbCodes::get("file")->load(TC_BASE."library/config/".$file)) {
			if(isset($this->_configStack[$out])) {
				return false;
			} else {
				$this->_configStack[$out] = $tmpConfig;
			
				return true;
			}
		} else {
			throw new Exception("Can't read config file");
		}
	}

	public function getConfiguration($name = null) {
		if(empty($name)) return false;

		if($test = $this->_configStack[$name]) {
			return $test;
		} else {
			throw new Exception("Can't find config: ".$cfg);
		}
	}

}