<?php

class Server {

	private $master;

	public function __construct() {
		
	}

	/**
	 * @public getSessions
	 *
	 * returns all sessions
	 *
	 * @return array
	 **/
	public function getSessions() {
		if(count($_SESSION) > 0) {
			return $_SESSION;
		} else {
			return false;
		}
	}

	/**
	 * @public getSssion
	 *
	 * return a specific session
	 *
	 * @return string
	 **/
	public function getSession($input) {
		if(count($_SESSION) > 0) {
			if(isset($_SESSION[$input])) {
				return $_SESSION[$input];
			}
		}
		
		return false;
	}

	/**
	 * @public setSession
	 *
	 * sets a session variable
	 *
	 * @return boolean
	 **/
	public function setSession(array $data, $replace = false) {
		$depth = Arrays::depth($data);

		foreach($depth as $key => $value) {
			if(isset($_SESSION[$key]) && $replace === false) {
				return false;
			} elseif($replace == true) {
				$_SESSION[$key] = $value;
			} else {
				$_SESSION[$key] = $value;
			}
		}

		return true;
	}

	public function existSession($name) {
		if(isset($_SESSION[$name])) {
			return true;
		}

		return false;
	}

	/**
	 * removes a specific session
	 *
	 * @param string $input
	 * @return bool
	 */
	public function removeSession($input = "") {
		if(empty($input)) return false;

		if(isset($_SESSION[$input])) {
			unset($_SESSION[$input]);

			return true;
		}

		return false;
	}

	public static function getCookie($request="") {
		if(empty($request)) return false;

		return self::$master->getCookie($request);
	}

	public static function setCookie($request="", $value="", $expire="") {
		if(
			empty($request)
		||
			empty($value)
		||
			empty($expire)
		) return false;
		
		self::$master->setCookie(
				$request,
				$value,
				$expire
			);
	}

	public static function setStatus($status = null) {
		if(empty($status) || !is_integer($status)) return false;

		self::$master->response->setStatus($status);
	}

	public static function getStatus() {
		return self::$master->response->getStatus();
	}
}