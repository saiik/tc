<?php

abstract class Controller {

	/**
	 * Main controller function.
	 * Will be called and used for the main output of this controller
	 *
	 * @return void
	 **/
	abstract public function init();

	/** 
	 * This function will be used for the package system.
	 * The Package System will locate any controller and get it's informations.
	 * It will check if all requirements are available and if there are any updates
	 *
	 * TBA - PARAMETER INFORMATIONS
	 * 
	 * @return array
	 **/
	abstract public function requestInformations();
}