<?php

class Render {

	private $smarty;

	public function __construct() {
		$this->smarty = app("smarty");
	}

	public function display() {

	}

	public function add($template = null) {
		if(empty($template)) return false;

		$this->smarty->assign("__content", $this->smarty->fetch($template));
	}

}