<?php

class Language {

	/**
	 * stores language | default de
	 *
	 * @param string
	 **/
	protected $_language = 'de'; 

	/**
	 * holds the language variables
	 *
	 * @param array
	 **/
	protected $_stack = [];

	/**
	 * holds the latest loaded stack
	 *
	 * @param string
	 **/
	protected $_latestStack;

	/**
	 * loads an language file
	 *
	 * @param string file
	 * @return void
	 **/
	public function loadLanguage($file = null) {
		if(empty($file)) return false;

		if(app("file")->check("language".DIRECTORY_SEPARATOR.$this->_language.DIRECTORY_SEPARATOR.$file)) {
			$this->_stack[$file] = app("file")->load("language".DIRECTORY_SEPARATOR.$this->_language.DIRECTORY_SEPARATOR.$file);
			$this->_latestStack = $file;
		} else {
			throw new Exception("Can't find language package: ".$file);
		}
	}

	/**
	 * returns the variable
	 *
	 * @param string stackName
	 * @param string varName
	 * @return string
	 **/
	public function get($stackName = null, $varName = null) {
		if(empty($stackName) || empty($varName)) return false;

		if(isset($this->_stack[$stackName])) {
			if(isset($this->_stack[$stackName][$varName])) {
				return $this->_stack[$stackName][$varName];
			} else {
				throw new Exception("Couldn't locate language variable: ".$varName);
			}
		} else {
			throw new Exception("Couldn't locate language stack: ".$stackName);
		}
	}

	/**
	 * returns the current language
	 *
	 * @return string
	 **/
	public function getLanguage() {
		return $this->_language;
	}

	/**
	 * sets the current language
	 *
	 * @param string lang
	 * @return void
	 **/
	public function setLanguage($lang = null) {
		if(empty($lang)) return false;

		if(!app("file")->check("language".DIRECTORY_SEPARATOR.$lang, false)) {
			return false;
		}

		$this->_language = (string)$lang;

		return true;
	}

	/**
	 * returns the latest loaded stack
	 *
	 * @return string
	 **/
	public function getLatestStack() {
		return $this->_latestStack;
	}
}