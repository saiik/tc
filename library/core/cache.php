<?php

class Cache {

	private $_path;

	public function __construct(array $config) {
		if(app('file')->check($config["database"], false)) {
			$this->_path = $config["database"];
		} else {
			throw new Exception("Please create the following dirs: ".$config["database"], "502");
		}
	}

}