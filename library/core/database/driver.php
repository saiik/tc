<?php

interface Driver {

	public function connect();

	public function select(array $select);

	public function from(array $from);

	public function leftJoin();

	public function innerJoin();

	public function rightJoin();

	public function where();

	public function whereIn();

	public function whereNotIn();

	public function order();

	public function limit();


}