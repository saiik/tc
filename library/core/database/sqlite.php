<?php

class SQLite implements Driver {

	private $host;
	private $user;
	private $pass;
	private $base;

	public function __construct(array $config) {
		$this->host = $config["host"];
		$this->user = $config["user"];
		$this->pass = $config["pass"];
		$this->base = $config["base"];
	
		$this->connect();
	}

	public function connect() {

	}

	public function select() {}

	public function from() {}

	public function leftJoin() {}

	public function innerJoin() {}

	public function rightJoin() {}

	public function where() {}

	public function whereIn() {}

	public function whereNotIn() {}

	public function order() {}

	public function limit() {}

}