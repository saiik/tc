<?php

class MySQL implements Driver {

	private $host;
	private $user;
	private $pass;
	private $base;
	private $connection;
	private $query;

	public function __construct(array $config) {
		$this->host = $config["host"];
		$this->user = $config["user"];
		$this->pass = $config["pass"];
		$this->base = $config["base"];
	
		try {
			$this->connect();
		} catch(Exception $e) {
			app("log")->addError('MySQL Error: '.$e->getMessage());
			die($e->getMessage());
		}
	}

	public function connect() {
		if(empty($this->connection)) {
			$this->connection = new MySQLi($this->host, $this->user, $this->pass, $this->base);
			if($this->connection->connect_error) {
				throw new Exception("Can't connect to database");
			}
		} else {
			throw new Exception("Database is already connected");
		}
	}

	public function select(array $select) {
		if(count($select) == 1) {
			$this->query = "SELECT ".$select[0];
		} else {
			$i = 0;
			$max = count($select) - 1;

			$this->query = "SELECT ";

			foreach($select as $value) {
				if($i < $max) {
					$this->query .= $value.", ";
				} else {
					$this->query .= $value;
				}
				$i++;
			}
		}

		return $this;
	}

	public function from(array $from) {
		
	}

	public function leftJoin() {}

	public function innerJoin() {}

	public function rightJoin() {}

	public function where() {}

	public function whereIn() {}

	public function whereNotIn() {}

	public function order() {}

	public function limit() {}

}