<?php

class Hooks {

	/**
	 * holds all functions for hooks
	 *
	 * @param array
	 **/
	private $_callbackStack = array();
	
	/**
	 * holds all hooks
	 *
	 * @param array
	 **/
	private $_hookStack = array();

	/**
	 * registers an specific hook
	 *
	 * @param string $hookName
	 * @return boolean
	 **/
	public function registerHook($hookName) {
		if(empty($hookName)) return false;

		if(in_array($hookName, $this->_hookStack)) {
			return false;
		}

		$this->_hookStack[] = $hookName;

		return true;
	}

	/**
	 * registers an event with callback function
	 *
	 * @param string $hookName
	 * @param callable $callable
	 * @return boolean
	 **/
	public function registerEvent($hookName, callable $callable) {
		if(empty($hookName) || empty($callable)) return false;

		if(is_callable($callable)) {
			if(in_array($hookName, $this->_hookStack)) {
				$this->_callbackStack[$hookName] = $callable;
			} else return false;
		}

		return false;
	}

	/** 
	 * fires a specific event
	 *
	 * @param string $hookName
	 * @return boolean
	 **/
	public function fireEvent($hookName) {
		if(in_array($hookName, $this->_hookStack)) {
			foreach($this->_callbackStack as $key => $value) {
				if($key == $hookName) {
					call_user_func($value);
				}
			}
		}

		return false;
	}

	/**
	 * displays hook stack and callback stack
	 *
	 * @return boolean | void
	 **/
	public function debug() {
		if(kernel()->isDebug()) {
			pretty($this->_hookStack);
			pretty($this->_callbackStack);
		} 

		return false;
	}
}