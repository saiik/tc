<?php

class Router {

	private $routes = [];
	private $basePath = "/play";
	private $methods = [
		"GET", 
		"POST", 
		"PUT",
		"DELETE"
	];
	private $matchTypes = [
		'int' => '[0-9]++',
		'all' => '[0-9A-Za-z]++'
	];

	public function route(array $route) {
		list($url, $callback, $type) = $route;

		if(empty($url) || empty($callback) || empty($type)) return false;

		if(!in_array($type, $this->methods)) {
			throw new Exception("Invalid request type", "502");
		}

		$this->routes[] = $route;

		return $this;
	}

	public function get(array $route) {
		list($url, $callback) = $route;

		return $this->route([$url, $callback, "GET"]);
	}

	public function post(array $route) {
		list($url, $callback) = $route;

		return $this->route([$url, $callback, "POST"]);
	}

	public function match($requestUrl = null, $requestMethod = null) {
		if($requestUrl == null) {
			$requestUrl = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '/';
		}

		$requestUrl = substr($requestUrl, strlen($this->basePath));

		if (($strpos = strpos($requestUrl, '?')) !== false) {
			$requestUrl = substr($requestUrl, 0, $strpos);
		}

		if($requestMethod === null) {
			$requestMethod = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : 'GET';
		}

		$_REQUEST = array_merge($_GET, $_POST);

		foreach($this->routes as $route) {
			list($url, $callback, $type) = $route;

			$callback = explode("::", $callback);
			$tmp = TumbCodes::set($callback[0], function() use($callback) {
				return new $callback[0]();
			});

			if($tmp === true || is_object($tmp)) {
				if($requestUrl == $url) {
					try {
						if($type == "GET") {
							$class = app($callback[0]);
							$class->$callback[1]($_GET);

							return;
						} elseif($type == "POST") {
							$class = app($callback[0]);
							$class->$callback[1]($_POST);

							return;
						}
					} catch(Exception $e) {

					}
				}
			} else {
				throw new Exception("Couldn't create ".$callback[0]);
			}
		}

		throw new Exception("Route handler not found");
	}
}