<?php

class Files implements FileSystem {

	public function load($file = null, $type = "php") {
		if(empty($file)) return false;

		if($this->check($file)) {
			$tmp = require_once($file.".".$type);

			return $tmp;
		} else {
			return false;
		}
	}

	public function check($file = null, $type = "php") {
		if(empty($file)) return false;

		$tmpFile = ($type==false?'':".".$type);

		if(file_exists($file.$tmpFile)) {
			return true;
		} else {
			return false;
		}
	}

}