<?php

class TumbCodes {

	/**
	 * holds the kernel
	 *
	 * @param object
	 **/
	private static $kernel;

	/**
	 * init the tumbcodes system
	 * and boot the kernel
	 *
	 * @param array $env
	 * @return void
	 **/
	public static function init(array $env) {
		self::$kernel = new Kernel($env);
		self::$kernel->boot();

		app("router")->match();
		app("render")->display();

		return true;
	}

	/**
	 * returns an single instance
	 *
	 * @param string $name
	 * @return object
	 **/
	public static function get($name = null) {
		if(self::$kernel instanceof Kernel) {
			return self::$kernel->singleton($name);
		} else {
			throw new Exception("Kernel not found", "502");
		}
	}

	/**
	 * creates a single instance
	 *
	 * @param string $name
	 * @param function $callback
	 * @return object
	 ***/
	public static function set($name = null, $callback = null) {
		if(is_callable($callback) AND !empty($name)) {
			return self::$kernel->make($name, $callback);
		} else {
			throw new Exception("Please provide correct parameters");
		}
	}

	/**
	 * returns the kernel
	 *
	 * @return object
	 **/
	public static function kernel() {
		if(self::$kernel instanceof Kernel) {
			return self::$kernel;
		} else {
			throw new Exception("Kernel not found", "502");
		}
	}
}