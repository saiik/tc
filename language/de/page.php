<?php

return [
	'login' => 'Anmelden',
	'profil' => 'Profil',
	'logout' => 'Abmelden',
	'loginNow' => 'Melde dich an',
	'registerNow' => 'Registriere dich jetzt',
	"register" => "Registrieren",
	'youAreHere' => 'Sie befinden sich hier: ',
];