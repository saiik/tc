<?php

return [
	'board' => 'Forum',
	'topics' => 'Beiträge',
	'lastA' => 'Letzte aktivität',
	'answers' => 'Antworten',
	'notopics' => 'Keine Themen vorhanden',
	'postsAm' => 'Beiträge:',
	'wroteAt' => 'geschrieben am',
	'reply' => 'Antworten',
];