<?php

return [
	'emptyFields' => 'Bitte geben Sie Ihren Benutzernamen und Passwort an',
	'error' => 'Fehler',
	'success' => 'Erfolgreich',
	"loginSuccess" => 'Sie haben sich erfolgreich angemeldet',
	'wrongPassword' => 'Das von Ihnen angegebe Passwort stimmt nicht',
	'userNotFound' => 'Der Benutzer wurde nicht gefunden',
	'errorLogout' => 'Es ist ein fehler beim Abmelden passiert, probieren Sie es nochmal',
];