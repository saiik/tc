<?php

return [
	'emptyFields' => 'Please enter your username and password',
	'error' => 'Error',
	'success' => 'Success',
	"loginSuccess" => 'You are logged in now',
	'wrongPassword' => 'Given password is wrong',
	'userNotFound' => 'Username not found',
	'errorLogout' => 'An error appeared while login out. Please try again',
];