<?php

return [
	'user' => 'User',
	'message' => 'Message',
	'messages' => 'Messages',
	'newMessage' => 'New message',
	'deleteMessage' => 'Delete message',
	'sendMessage' => 'Send message',
	'setLabel' => 'Set label',
	'deleteLabel' => 'Delete label',
	'newLabel' => 'Create new label',
	'noMessages' => "No messages available",
	'postEmptyLabel' => 'Please give an label name',
	'postEmptyColor' => 'Please select a color',
	'postLabelSave' => 'Label has been deleted',
	'postLabelSaveFail' => 'Label couldn\'t get saved',
	'postLabelEmpty' => 'Please give some input',
	'messageReciverNoExist' => 'Your selected user doesn\’t exists',
	'postMessageNoUser' => 'Please enter a username',
	'postMessageNoTitle' => 'Please enter a title',
	'postMessageNoContent' => 'Please enter a message content',
	'postMessageSave' => 'Message has been sent',
	'postMessageSaveFail' => 'Message couldn\'t get send',
	'messageDelete' => 'Message has been deleted',
	'messageDeleteFail' => 'Message couldn\'t get deleted',
	'labelDelete' => 'Label has been deleted',
	'labelDeleteFail' => 'Label couldn\'t get deleted',
	'labelSave' => 'Label has been saved',
	'labelSaveFail' => 'Label couldn\'t get saved',
	'wroteBy' => 'message from',
	'title' => 'Title',
	'date' => 'Date',
	'inbox' => 'Inbox',
	'out' => 'Send',
	'trash' => 'Trash',
	'newLabelShort' => 'New label',
	'delete' => 'delete',
	'selectLabel' => 'Select an label',
	'noLabels' => 'No labels available',
	'report' => 'Report',
	'forward' => 'Fastforward',
	'reply' => 'Reply:',
	'content' => 'Content:',
	'back' => 'Back',
	'save' => 'Save',
	'green' => 'Green',
	'red' => 'Red',
	'blue' => 'Blue',
	'lightblue' => 'Lightblue',
	'orange' => 'Orange',
	'reciever' => 'Username:',
	'send' => 'Send',
	'redirect' => 'You will be redirect in 3 seconds',
	'userRole' => 'User group',
	'noUserRole' => 'No user group',
	'comments' => 'Comments',
	'contact' => 'Contact',
	'yesterday' => 'Yesterday'
];