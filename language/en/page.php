<?php

return [
	'login' => 'Login',
	'profil' => 'Profile',
	'logout' => 'Logout',
	'loginNow' => 'Login now!',
	'registerNow' => 'Register now!',
	'register' => 'Register',
	'youAreHere' => 'You are here: ',
];